### What assumptions did you make?
- You can create/edit profile and logout
- small end build,
- smaller packages = better
- smaller and lesser quality images for mobile
- single sass file, easy to maintain in simple app
- no inside component styles
- too small and not reusable app to do state/stateless components, but in real project would definitely think about it.
- keeping modules separate(services, store(state), components, hooks etc.

### Why did you decide to use specific techniques?
- Atlaskit's component are build with TypScript and React, so it was obvious to use both of them.
- I used create-react-app because I consider it as a good start for such small presentations/projects.You got everything out of the box, just added TS.
- I think that considering simple design it would be better not to use complex state management like MobX. I read about Zustand, check some simple hook examples and it works/looks really cool. Simple hooks for app's state.
- Installed react-router for fast history/SPA view changes.
- I wanted User Entity to be a DDD thing, so that new is only used for creating and for editing/reading you need to hydrate the model. That's why I used TypedJSON
- I think that in real project Service should be responsible for creating new Entity instance but I didn't want to push so many params in 3 files, so I have left it in store.
- Stored user's data in localStorage, so it will persist

### What could be the alternative solutions?
- using cookies to make it session persistent
- using pure React useState

### Any other comments you think we should know when assessing your solution?
- Atlaskit's documentation should get an update.
- Atlaskit's form validation is poor and would definitely go for something that will wrap those input and form(like bigger Observer(Form) and Providers for each field)
- Good that all fields/component are a separate packages, lowers build.
- I did not make any tests because it was not pointed in the assignment but some simple component test could be done + functional test for UserService.
- Moving localStorage methods to some helper with json parse could be an option for wider use and some tests.