import { jsonObject, jsonMember } from 'typedjson';

@jsonObject()
export default class User {

  private _firstName: string;
  private _lastName: string;
  private _email: string;
  private _street: string;
  private _isInhabitant: boolean;
  private _about: string|null;

  public constructor(
    firstName: string,
    lastName: string,
    email: string,
    street: string,
    isInhabitant: boolean,
    about: string|null = null
  ) {
    this._firstName = firstName;
    this._lastName = lastName;
    this._email = email;
    this._street = street;
    this._isInhabitant = isInhabitant;
    this._about = about;
  }

  @jsonMember({ constructor: String })
  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  @jsonMember({ constructor: String })
  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  @jsonMember({ constructor: String })
  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  @jsonMember({ constructor: String })
  get street(): string {
    return this._street;
  }

  set street(value: string) {
    this._street = value;
  }

  @jsonMember({ constructor: Boolean })
  get isInhabitant(): boolean {
    return this._isInhabitant;
  }

  set isInhabitant(value: boolean) {
    this._isInhabitant = value;
  }

  @jsonMember({ constructor: String })
  get about(): string | null {
    return this._about;
  }

  set about(value: string | null) {
    this._about = value;
  }
}