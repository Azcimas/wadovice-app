export const lengthValidator = (
  value: string,
  minLength?: number,
  maxLength?: number
) => {
  if (minLength && value.length < minLength) {
    return `Field should have at least ${minLength} characters`;
  } else if (maxLength && value.length > maxLength) {
    return `Field should have at most ${maxLength} characters`;
  }
  return undefined;
};

export const emailValidator = (value: string) => {
  const regexp = new RegExp([
    '^(([^<>()[\\]\\\\.,;:\\s@"]+(\\.[^<>()[\\]\\\\.,;:\\s@"]+)*)',
    '|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}',
    '\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$'
  ].join(''));

  return regexp.test(value.toLowerCase()) ?
    undefined :
    'This is not a valid e-mail address';
};