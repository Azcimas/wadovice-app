import create from 'zustand';
import User from 'entities/User';
import UserService from 'services/UserService';

type State = {
  user: User|null,
  create: (
    firstName: string,
    lastName: string,
    email: string,
    street: string,
    isInhabitant: boolean,
    about: string|null,
  ) => void,
  edit: (
    firstName: string,
    lastName: string,
    email: string,
    street: string,
    isInhabitant: boolean,
    about: string|null,
  ) => void,
  get: () => void,
  remove: () => void,
}

/**
 * Store with User's data
 */
export const userStore = create<State>((set, get) => ({
  user: null,
  create: async (firstName, lastName, email, street, isInhabitant, about) => {
    const user: User = new User(
      firstName,
      lastName,
      email,
      street,
      isInhabitant,
      about,
    );
    const response = await UserService.add(user);
    response && set(() => ({ user: user }));
  },
  edit: async (firstName, lastName, email, street, isInhabitant, about) => {
    const user = get().user;
    if (user) {
      user.firstName = firstName;
      user.lastName = lastName;
      user.email = email;
      user.street = street;
      user.isInhabitant = isInhabitant;
      user.about = about;

      const response = await UserService.add(user);
      response && set(() => ({ user: user }));
    } else {
      throw new Error('No user found');
    }
  },
  get: async () => {
    const user = await UserService.get();
    user && set(() => ({ user: user }));
  },
  remove: () => {
    UserService.remove();
    set(() => ({ user: null }));
  }
}));