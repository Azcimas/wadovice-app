import User from 'entities/User';
import { TypedJSON } from 'typedjson';
import { USER } from 'conts/localStorage';

export default {
  /**
   * Saving user's data to storage
   */
  add(user: User): Promise<Boolean> {
    return new Promise((resolve, reject) => {
      try {
        const serializer = new TypedJSON(User);
        const userJSON = serializer.stringify(user);
        window.localStorage.setItem(USER, userJSON);
        resolve(true);
      } catch (error) {
        console.log(error);
        reject(false);
      }
    });
  },

  /**
   * Gets user's data from storage
   */
  get(): Promise<User|null> {
    return new Promise((resolve, reject) => {
      try {
        const userJson = window.localStorage.getItem(USER);
        if (!userJson) {
          resolve(null);
        }
        const serializer = new TypedJSON(User);
        const user = serializer.parse(userJson);
        resolve(user || null);
      } catch (error) {
        console.log(error);
        reject(null);
      }
    });
  },

  /**
   * Removes user data from storage
   */
  remove(): void {
    window.localStorage.removeItem(USER);
  },
};