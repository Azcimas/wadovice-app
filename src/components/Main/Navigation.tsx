import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import {
  AtlassianNavigation,
  CustomProductHome,
  PrimaryButton,
  Profile
} from '@atlaskit/atlassian-navigation';
import Logo from 'assets/nav/logo.png';
import Icon from 'assets/nav/logo-small.png';
import ProfileIcon from 'assets/nav/profile.png';
import { userStore } from 'store/user';

const CustomHome = () =>
  <CustomProductHome
    href="/"
    iconAlt="Wadovice"
    iconUrl={Icon}
    logoAlt="Wadovice"
    logoUrl={Logo}
    siteTitle="Welcome"
  />
;

const primaryItems = [
  <Link to="/" style={{ textDecoration: 'none', }}>
    <PrimaryButton>
      Home
    </PrimaryButton>
  </Link>,
  <Link to="/user-info" style={{ textDecoration: 'none', }}>
    <PrimaryButton>
      User Information
    </PrimaryButton>
  </Link>,
  <Link to="user-form" style={{ textDecoration: 'none', }}>
    <PrimaryButton>
      User Form
    </PrimaryButton>
  </Link>
];

const UserProfile = () => {
  const user = userStore(state => state.user);
  const removeUser = userStore(state => state.remove);
  const history = useHistory();

  const logout = () => {
    removeUser();
    history.push('home');
  };

  if (!user) {
    return (
      <Link to="/user-form" style={{ textDecoration: 'none' }}>
        Create profile
      </Link>
    );
  } else {
    return (
      <Profile
        icon={<img src={ProfileIcon} />}
        tooltip="Logout!"
        onClick={logout}
      />
    );
  }

};

export default () => {
  return (
    <AtlassianNavigation
      label="wadovice"
      renderProductHome={CustomHome}
      primaryItems={primaryItems}
      renderProfile={UserProfile}
    />
  );
};