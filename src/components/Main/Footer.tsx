import React, { Component, ReactNode } from 'react';

interface FooterProps {
  left?: ReactNode;
  center?: ReactNode;
  right?: ReactNode;
}

export default class Footer extends Component<FooterProps> {
  render() {
    return (
      <footer className="wado_footer">
        <div className="footer-left">
          {this.props.left}
        </div>
        <div className="footer-center">
          {this.props.center}
        </div>
        <div className="footer-right">
          {this.props.right}
        </div>
      </footer>
    );
  }
}