import React from 'react';
import Welcome from 'assets/home/welcome.jpg';
import WelcomeMobile from 'assets/home/welcome-mobile.jpg';
import { userStore } from 'store/user';

export default () => {
  const user = userStore(state => state.user);

  return (
    <div className="wado_content">
      <div className="wado_header">
        {`Welcome ${user ? user.firstName : 'Stranger'} to`}
      </div>
      <picture>
        <source srcSet={Welcome} media="(min-width: 800px)" />
        <img
          className="wado_welcome-picture"
          srcSet={WelcomeMobile}
          alt="example"
        />
      </picture>
    </div>
  );
};