import React, { Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import Form, {
  CheckboxField,
  ErrorMessage,
  Field,
  FormHeader,
  FormFooter
} from '@atlaskit/form';
import Button from '@atlaskit/button/standard-button';
import { Checkbox } from '@atlaskit/checkbox';
import Select, { ValueType } from '@atlaskit/select';
import TextArea from '@atlaskit/textarea';
import TextField from '@atlaskit/textfield';
import { STREETS } from 'conts/streets';
import { emailValidator, lengthValidator } from 'validators';
import { userStore } from 'store/user';

interface OptionType {
  label: string;
  value: string;
}

interface FormTypes {
  isInhabitant: Array<string>;
  firstName: string;
  lastName: string;
  email: string;
  about: string|null;
  street: OptionType;
}

const validateFirstName = (value: string = ''): string|undefined => {
  return lengthValidator(value, 3, 50);
};

const validateLastName = (value: string = ''): string|undefined  => {
  return lengthValidator(value, 3, 100);
};

const validateEmail = (value: string = ''): string|undefined  => {
  const lenCheck = lengthValidator(value, 6);
  if (lenCheck === undefined) {
    return emailValidator(value);
  }
  return lenCheck;
};

export default () => {
  const [user, createUser, editUser] = userStore(state => [
    state.user,
    state.create,
    state.edit
  ]);
  const history = useHistory();

  const submit = (data: FormTypes) => {
    if (user) {
      editUser(
        data.firstName,
        data.lastName,
        data.email,
        data.street ? data.street.value : '',
        !!data.isInhabitant.length,
        data.about,
      );
    } else {
      createUser(
        data.firstName,
        data.lastName,
        data.email,
        data.street ? data.street.value : '',
        !!data.isInhabitant.length,
        data.about,
      );
    }
    history.push('user-info');
  };

  const defaultStreet = user ?
    STREETS.find(s => s.value === user.street) :
    STREETS[0]
  ;

  return (
    <div className="wado_content">
      <Form<FormTypes> onSubmit={submit}>
        {({ formProps }) => (
          <form {...formProps}>
            <FormHeader title={`${user ? 'Change' : 'Fill'} your info...`} />
            <CheckboxField
              name="isInhabitant"
              value="1"
              defaultIsChecked={user ? user.isInhabitant : false}
            >
              {({ fieldProps }) => (
                <Checkbox
                  {...fieldProps}
                  size="large"
                  label="Wadovice inhabitant?"
                />
              )}
            </CheckboxField>
            <Field
              name="firstName"
              defaultValue={user ? user.firstName : ''}
              label="First name"
              isRequired
              validate={validateFirstName}
            >
              {({ fieldProps, error }) =>
                <Fragment>
                  <TextField {...fieldProps} />
                  {error && <ErrorMessage>{error}</ErrorMessage>}
                </Fragment>
              }
            </Field>
            <Field
              name="lastName"
              defaultValue={user ? user.lastName : ''}
              label="Last name"
              isRequired
              validate={validateLastName}
            >
              {({ fieldProps, error }) =>
                <Fragment>
                  <TextField {...fieldProps} />
                  {error && <ErrorMessage>{error}</ErrorMessage>}
                </Fragment>
              }
            </Field>
            <Field
              name="email"
              defaultValue={user ? user.email : ''}
              label="E-mail"
              isRequired
              validate={validateEmail}
            >
              {({ fieldProps, error }) =>
                <Fragment>
                  <TextField {...fieldProps} />
                  {error && <ErrorMessage>{error}</ErrorMessage>}
                </Fragment>
              }
            </Field>
            <Field<ValueType<OptionType, false>>
              label="Favourite street?"
              name="street"
              defaultValue={defaultStreet}
            >
              {({ fieldProps: { id, ...rest } }) => (
                <Select<OptionType>
                  id={`${id}-select`}
                  isSearchable={false}
                  options={STREETS}
                  {...rest}
                />
              )}
            </Field>
            <Field<string, HTMLTextAreaElement>
              name="about"
              defaultValue={user && user.about ? user.about : ''}
              label="About me"
            >
              {({ fieldProps }) => <TextArea {...fieldProps} />}
            </Field>

            <FormFooter>
              <Button type="submit" appearance="primary">
                Submit
              </Button>
            </FormFooter>
          </form>
        )}
      </Form>
    </div>
  );
};
