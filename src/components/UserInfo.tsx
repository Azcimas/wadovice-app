import React from 'react';
import { Link } from 'react-router-dom';
import Avatar from '@atlaskit/avatar';
import Badge from '@atlaskit/badge';
import Button from '@atlaskit/button/standard-button';
import { userStore } from 'store/user';

export default () => {
  const user = userStore(state => state.user);
  if (user) {
    return (
      <summary className="wado_content thin">
        <div className="wado_avatar">
          <Avatar size="xxlarge" />
        </div>
        <div>
          { user.isInhabitant &&
            <Badge appearance="primary">Inahabitant</Badge>
          }
        </div>
        <div>
          <p>Name:</p>
          <p>{user.firstName} {user.lastName}</p>
        </div>
        <div>
          <p>E-mail:</p>
          <p>{user.email}</p>
        </div>
        <div>
          <p>Favourite street:</p>
          <p>{user.street}</p>
        </div>
        <div>
          <p>About:</p>
          <p>{user.about || '...'}</p>
        </div>
      </summary>
    );
  } else {
    return (
      <div className="wado_content">
        <div className="fill-info">
          <Link to="/user-form" style={{ textDecoration: 'none', }}>
            <Button appearance="primary">
              Fill your info!
            </Button>
          </Link>
        </div>
      </div>
    );
  }
};
