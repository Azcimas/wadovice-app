import React, { Component, Fragment } from 'react';
import Navigation from 'components/Main/Navigation';
import Footer from 'components/Main/Footer';

const LoremIpsum =
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
  </p>
;

export default class Main extends Component {
  render() {
    return (
      <Fragment>
        <Navigation />
        <main>
          {this.props.children}
        </main>
        <Footer
          left={LoremIpsum}
          center={LoremIpsum}
          right={LoremIpsum}
        />
      </Fragment>
    );
  }
}