import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import './App.sass';
import Home from 'components/Home';
import UserInfo from 'components/UserInfo';
import UserForm from 'components/UserForm';
import Main from 'components/Main';
import { userStore } from 'store/user';

export default () => {
  const getUser = userStore(state => state.get);
  getUser();
  return (
    <div className="wado_page-container">
      <Router>
        <Main>
          <Switch>
            <Route path="/user-info">
              <UserInfo />
            </Route>
            <Route path="/user-form">
              <UserForm />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Main>
      </Router>
    </div>
  );
};
